/** @type {import("prettier").Config & import("@trivago/prettier-plugin-sort-imports").PluginConfig} */
const config = {
  plugins: [
    '@trivago/prettier-plugin-sort-imports',
    'prettier-plugin-tailwindcss',
  ],

  trailingComma: 'all',
  tabWidth: 2,
  semi: true,
  singleQuote: true,

  /* prettier sort import settings */
  importOrder: [
    /* for vite config */
    '^vite$',
    '^vite-',

    /* for simple source */
    '^solid-js$',
    '^@components/(.*)$',
    '^[./]',
  ],
  importOrderSeparation: true,
};

export default config;
