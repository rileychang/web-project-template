import { defineConfig } from 'vite';

import solid from 'vite-plugin-solid';
import tsConfigPath from 'vite-tsconfig-paths';

export default defineConfig({
  plugins: [tsConfigPath(), solid()],
});
