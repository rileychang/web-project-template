import { Component, createSignal } from 'solid-js';

const Counter: Component = () => {
  const [count, setCount] = createSignal(0);
  const increase = () => setCount((i) => i + 1);

  return (
    <div>
      <p>Count: {count()}</p>
      <button
        onClick={() => {
          increase();
        }}
      >
        Increase
      </button>
    </div>
  );
};

export default Counter;
