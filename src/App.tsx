import { Component } from 'solid-js';

import Counter from '@components/Counter';

const App: Component = () => {
  return (
    <div>
      <Counter />
    </div>
  );
};

export default App;
