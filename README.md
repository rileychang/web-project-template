# Web Project Template

A personal template of browser application, which use:

- **Nix** (Flake) to manage Javascript runtime and develop environment
- **pnpm** to manage Javascript packages
- **Vite** to build and bundle browser-side application
- **SolidJS** as UI framework
- **TailwindCSS** as CSS framework
- **Prettier** as formmater

## More...

I don't use linter in a personal project.
