{
  description = "Web Project Template";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  } @ inputs: (flake-utils.lib.eachDefaultSystem (system: let
    pkgs = import nixpkgs {
      inherit system;
    };

    nixUtils = with pkgs; [
      alejandra
    ];

    jsUtils = with pkgs;
      [
        nodejs
      ]
      ++ (with pkgs.nodePackages; [
        npm
        pnpm
        prettier
      ]);

    vscodium = pkgs.vscode-with-extensions.override {
      vscode = pkgs.vscodium;
      vscodeExtensions = with pkgs.vscode-extensions; [
        # language pack
        ms-ceintl.vscode-language-pack-zh-hant

        # nix support
        jnoortheen.nix-ide

        # js support
        esbenp.prettier-vscode
      ];
    };
  in {
    formatter = pkgs.alejandra;

    devShells.default = pkgs.mkShell {
      packages = nixUtils ++ jsUtils ++ [vscodium];

      # install node package after entering dev shell
      shellHook = ''
        pnpm install
      '';
    };
  }));
}
